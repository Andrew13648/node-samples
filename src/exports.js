// Examples of how to export classes.  Use in conjunction with require.js.
class Employee {
    constructor(name) {
        this.name = name
    }
}

class Animal {
    constructor(species) {
        this.species = species
    }
}

module.exports = {
    Employee,
    Animal
}