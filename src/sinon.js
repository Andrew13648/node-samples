// See https://sinonjs.org/releases/latest/stubs/ for more stub examples
const sinon = require("sinon")

const o = {
    f: function () {
        console.log("inside f")
        this.g()
    },
    g: function () {
        console.log("inside g")
    }
}

console.log(o)


// Stubs can override object methods or be anonymous with sinon.stub()
// Anonymous example
let stub = sinon.stub()
stub.returns("world")
console.log(stub("hello")) // world
console.log(stub.firstCall.args) // [ 'hello' ]
console.log(stub.called) // true
console.log("calledOnce", stub.calledOnce) // true
console.log("callCount", stub.callCount) // 1
console.log(stub.getCall(0).args) // [ 'hello' ]
stub.resetHistory()
console.log(stub.called) // false

// Replaces the g method on o 
stub = sinon.stub(o, "g")
o.f() // note that inside g is not printed since it was stubbed
console.log(stub.called) // true

stub.restore() // Restores the originally function back to o
o.f() // Prints inside f, inside g

stub = sinon.stub(o, "g")
stub.throws(Error("sample error"))
try {
    o.f()
} catch (error) {
    console.log("caught error from stub", error)
}

// Fakes - behavior cannot be changed after creation
let fake = sinon.fake() // Creates a simple fake that can be called
fake()
fake = sinon.fake.returns("hello from fake") // Adds behavior
console.log(fake("hello", "sample arg"))
// The next line logs a double array because the first layer is the call number.
console.log("args", fake.args) // logs [ [ "hello", "sample arg " ] ]
// The next line would be equivalent to fake.getCall(0).args
console.log("first arg", fake.args[0]) // logs [ "hello", "sample arg " ]
console.log(fake.lastArg) // sample arg
console.log(fake.getCall(0).lastArg) // sample arg
console.log(fake.callCount) // 1
console.log(fake.calledOnce) // true
fake = sinon.fake.throws(Error("sample error"))
try {
    fake()
} catch (error) {
    console.log("caught error from fake", error)
}



