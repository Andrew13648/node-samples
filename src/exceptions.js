// Simple examples of JavaScript exception handling
console.log("Beginning execution...")

class ValidationError extends Error {
    constructor(message) {
        super(message)
        this.name = "ValidationError"
    }
}

function throwString() {
    throw "error string"
}

function throwError() {
    throw new Error("error object")
}

function throwValidationError() {
    throw new ValidationError("validation error")
}

function tryCatchFinally(f) {
    try {
        console.log(`Invoking ${f.name}`)
        f()
    }
    catch (e) {

        console.log(`Caught exception for ${f.name}:`, e)

        // Check for a particular type of error
        if (e instanceof ValidationError) {
            console.log("e is a validation error (class check)")
        }

        // You can also check the name if you don't have access to the class
        if (e.name === "ValidationError") {
            console.log("e is a validation error (name check)")
        }
    }
    finally {
        console.log(`Inside finally for ${f.name}`)
    }
}

tryCatchFinally(throwString)
tryCatchFinally(throwError)
tryCatchFinally(throwValidationError)
