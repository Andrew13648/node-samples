// Examples of destructuring and the spread operator
let arr = [1, 2, 3, [4]]

let [a, b, c, [d]] = arr

console.log(a)
console.log(b)
console.log(c)
console.log(d)

const f = (a, b, c) => {
    console.log(a)
    console.log(b)
    console.log(c)
}

f(...arr)

const f2 = ([, b, ...c]) => {
    console.log("f2, b", b)
    console.log("f2, c", c)
}

f2(arr)

/* Example of how to use destructuring to calculate the determinant 
of a 2x2 matrix */
const determinant = ([[a, b], [c, d]]) => a * d - b * c

console.log(determinant([[5, 3], [-2, 1]]))
