async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

describe("test", () => {
    it("should resolve long", async () => {
        console.log("before await in test long")
        await sleep(5000)
        console.log("after await in test long")
    })

    it("should resolve quick", async () => {
        console.log("before await in test short")
        await sleep(100)
        console.log("after await in test short")
    })
})

before(function () {
    console.log("before")
})

beforeEach(function () {
    console.log("before each")
})

after(function () {
    console.log("after")
})

afterEach(function () {
    console.log("after each")
})


