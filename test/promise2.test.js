const sinon = require("sinon")
const proxyquire = require("proxyquire")
const assert = require("assert")

describe("promise2.js", () => {
    describe("getValuePromise()", () => {
        it("resolves values over 1000 using require", async () => {

            const getValuePromise = require("../src/promise2")
            const res = getValuePromise(5000)
            console.log("result", res) // Logs Promise { <pending> }
            await res
            console.log("result", res) // Logs Promise { 5000 }
            // The value of the promise can be accessed by using then...
            await res.then((val) => {
                assert.strictEqual(val, 5000)
            })
            // Instead, the result could have been set to the resolved value:
            // res = await res            
        })

        it("resolves values over 1000 using proxyquire", async () => {

            const fake = sinon.fake.yields(4000)
            const getValuePromise = proxyquire("../src/promise2", {
                "./callback": {
                    callbackWithValue: fake
                }
            })
            const res = await getValuePromise(5000)
            assert.strictEqual(res, 4000)
            assert.strictEqual(fake.args[0][0], 5000)
        })

        it("rejects values <= 1000 using proxyquire", async () => {

            const getValuePromise = proxyquire("../src/promise2", {
                "./callback": {
                    callbackWithValue: sinon.fake.yields(850)
                }
            })
            try {
                await getValuePromise(5000)
                assert.fail("An error was expected, but none was thrown")
            }
            catch (error) {
                assert.strictEqual(error.message, "val is 850")
            }
        })
    })
})
